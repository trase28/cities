<?php
/**
 * Created by PhpStorm.
 * User: trase
 * Date: 24.04.17
 * Time: 17:32
 */

namespace app\components;


class Translit
{
    /**
     * @var string[] Replace list. Most friendly with Google and Yandex.
     */
    private static $_chr = [
        '0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4',
        '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9',
        'a' => 'a', 'b' => 'b', 'c' => 'c', 'd' => 'd', 'e' => 'e',
        'f' => 'f', 'g' => 'g', 'h' => 'h', 'i' => 'i', 'j' => 'j',
        'k' => 'k', 'l' => 'l', 'm' => 'm', 'n' => 'n', 'o' => 'o',
        'p' => 'p', 'q' => 'q', 'r' => 'r', 's' => 's', 't' => 't',
        'u' => 'u', 'v' => 'v', 'w' => 'w', 'x' => 'x', 'y' => 'y',
        'z' => 'z',
        'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd',
        'е' => 'e', 'ё' => 'yo', 'ж' => 'zh', 'з' => 'z', 'и' => 'i',
        'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n',
        'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
        'у' => 'u', 'ф' => 'f', 'х' => 'kh', 'ц' => 'ts', 'ч' => 'ch',
        'ш' => 'sh', 'щ' => 'shh', 'ъ' => '"', 'ы' => 'y', 'ь' => '\'',
        'э' => 'eh', 'ю' => 'yu', 'я' => 'ya',
        '-' => '-', ' ' => ' ', '.' => '-', ',' => '-',
    ];
    private static $_replace = [
        'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd',
        'е' => 'e', 'ё' => 'yo', 'ж' => 'zh', 'з' => 'z', 'и' => 'i',
        'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n',
        'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
        'у' => 'u', 'ф' => 'f', 'х' => 'kh', 'ц' => 'ts', 'ч' => 'ch',
        'ш' => 'sh', 'щ' => 'shh', 'ъ' => '"', 'ы' => 'y', 'ь' => '\'',
        'э' => 'eh', 'ю' => 'yu', 'я' => 'ya',
        '-' => '-', ' ' => ' ', '.' => '-', ',' => '-',
    ];
    /**
     * Tranlit.
     * @param string $text Text for transliteration.
     * @return string
     */
    public static function t($text)
    {
        $text = mb_strtolower($text);
        //cyrilic and symbols translit
        $replace = self::$_replace;
        $s = '';
        for ($i = 0; $i < mb_strlen($text); $i++) {
            $c = mb_substr($text, $i, 1);
            if (array_key_exists($c, $replace)) {
                $s .= $replace[$c];
            } else {
                $s .= $c;
            }
        }
        //other translit
        //make shure that you set locale for using iconv
        //$s = iconv('UTF-8', 'ASCII//TRANSLIT', $s);

        //remove symbols
        $s = preg_replace('/[^\-0-9a-z]+/i', '', $s);
        //double spaces
        $s = preg_replace('/\-+/', '-', $s);
        //spaces at begin and end
        $s = preg_replace('/^\-*(.*?)\-*$/', '$1', $s);
        return $s;
    }

    public static function r($text){
        $text = mb_strtolower($text);
        $replace = array(
            "й","ц","у","к","е","н","г","ш","щ","з","х","ъ",
            "ф","ы","в","а","п","р","о","л","д","ж","э",
            "я","ч","с","м","и","т","ь","б","ю"
        );
        $search = array(
            "q","w","e","r","t","y","u","i","o","p","[","]",
            "a","s","d","f","g","h","j","k","l",";","'",
            "z","x","c","v","b","n","m",",","."
        );
        return str_replace($search, $replace, $text);
    }
}