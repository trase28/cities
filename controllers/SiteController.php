<?php

namespace app\controllers;

use app\components\ApiInterface;
use app\components\Translit;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\grid\GridView;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Cities;


class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $page = Yii::$app->request->get('page', 1);
        $pageSize = Yii::$app->request->get('per-page', 25);
        $query = Cities::find()
            ->select('cities.*, countries.rus')
            ->leftJoin('countries', 'cities.country_id = countries.isoID')
            ->groupBy(new Expression('TRIM(UPPER(`original`))'))
            ->orderBy(new Expression("TRIM(original)"))
            ->asArray()->all();
        Cities::$citiesDb = $query;
        $query = array_filter($query, function($var){
            return ($var['corrected'] == null)?  true : false;
        });
        $i = 0;
        foreach ($query as $item){
            $reIndexed[$i] = $item;
            $i++;
        }
        $query = $reIndexed; unset($i); unset ($reIndexed);
        for ($i = $pageSize*$page-$pageSize; $i < $pageSize*$page; $i++){
            $suggestions = Cities::getSuggestions($query[$i]['original'], $query[$i]['country_id']);
            if ($suggestions != []) $query[$i]['suggestions'] = $suggestions;
                else $query[$i]['suggestions'][0]['original'] = "НЕИЗВЕСТНЫЙ ГОРОД";
        }
        $geo = new ApiInterface();
        $provider = new ArrayDataProvider([
            'allModels' => $query,
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);
        $grid = GridView::widget([
            'dataProvider' => $provider,
            'tableOptions' => [
                'class' => 'table table-striped table-bordered'
            ],
            'columns' => [
                [
                    'format' => 'text',
                    'label' => 'Страна',
                    'attribute' => 'rus',
                ],
                [
                    'format' => 'text',
                    'label' => 'Как в базе',
                    'content' => function ($model, $key, $index, $column) {
                        $title = "";
                        $title .= '<span class="original" id="orig-';
                        $title .= $model['id'];
                        $title .= '">';
                        $title .= $model['original'];
                        $title .= '</span>';
                        return $title;
                    },
                ],
                [
                    'format'=> 'text',
                    'label' => 'Как будет',
                    'contentOptions' => ['style' => 'min-width: 200px; width: 200px;'],
                    'content' => function ($model, $key, $index, $column) {
                        $inp = "";
                        $inp .= '<input type="text" id="inp-';
                        $inp .= $model['id'];
                        $inp .= '" class="form-control city-input" value="';
                        $inp .= $model['suggestions'][0]['original'];
                        $inp .= '">';
                        return $inp;
                    },
                ],
                [
                    'format'=> 'text',
                    'label' => 'Что говорит гугл',
                    'content' => function ($model, $key, $index, $column) use ($geo) {
                        $span = '<span id="';
                        $span .= "ggl-".$model['id'];
                        $span .= ' "class = "span-ggl"> Гуглю за тебя... </span>';
                        return $span;
                    },
                ],
                [
                    'format' => 'text',
                    'label' => 'Предложения',
                    'content' => function ($model, $key, $index, $column){
                        $keySet = "";
                        foreach ($model['suggestions'] as $suggestion){
                            $keySet .= '<button class="btn btn-info" onclick="editInput(';
                            $keySet .= $model['id'];
                            $keySet .= ", '";
                            $keySet .= $suggestion['original'];
                            $keySet .= "');\">";
                            $keySet .= $suggestion['original'];
                            $keySet .= '</button>';
                            $keySet .= '&nbsp';
                        }
                        return $keySet;
                    },
                ],
                [
                    'label' => 'Действия',
                    'format' => 'text',
                    'content' =>  function ($model, $key, $index, $column){
                        $buttonSet = "";
                        $buttonSet .= ' <button class="btn btn-success btn-save" onclick="save(';
                        $buttonSet .= $model['id'];
                        $buttonSet .= ');">Сохранить</button>';
                        $buttonSet .= "&nbsp";
                        $buttonSet .= '<button class="btn btn-danger btn-save" onclick="leaveAsIs(';
                        $buttonSet .= $model['id'];
                        $buttonSet .= ');">Оставить как было</button>';

                        return $buttonSet;
                    },
                ],
            ],
        ]);
        return $this->render('index',[
            'grid' => $grid
        ]);
    }

    public function actionGoogle(){
        $query = Yii::$app->request->post('q');
        $gglRes = (new ApiInterface())->findGoogle($query);
        return Translit::t($gglRes);
    }
    public function actionSave(){
        $id = (int) Yii::$app->request->post('id');
        $val = htmlspecialchars(Yii::$app->request->post('val'));
        Cities::saveSingle($id, $val);
    }
    public function actionSaveall(){
        $rawJSON = Yii::$app->request->post('data');
        Cities::saveFromJSON($rawJSON);
    }

    public function actionUsers(){


    }
}
