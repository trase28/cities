
function editInput(id, val){
    $("#inp-"+id).val(val);
}

function save(id) {
    var val = $("#inp-"+id).val();
    $("#inp-"+id).removeClass("green-input");
    $.ajax({
        url: "/save",
        type: "POST",
        data: {'id': id, 'val': val},
        success: function () {
            $("#inp-"+id).addClass("green-input");
        }
    });
}

function leaveAsIs(id){
    var orig = $("#orig-"+id).html();
    orig = $.trim(orig);
    $("#inp-"+id).val(orig);
    save(id);
}

function saveAll(){
    var collection = $(".city-input");
    var data = {};

    for (var i = 0; i < collection.length; i++){
        var item = collection[i];
        data[i] = {};
        var rawId = $(item).attr("id");
        rawId = parseInt(rawId.replace(/\D+/g,""));
        data[i]['id'] = rawId;

        data[i]['val'] = $(item).val();
    }
    data = JSON.stringify(data);
    $.ajax({
        url: "/saveall",
        type: "POST",
        data: {data: data},
        success: function (data) {
            window.location.reload();
        }
    });
}

$(document).ready(function () {
    $(".span-ggl").each(function (index, element) {
        var span_id = $(element).attr("id");
        var rawId = parseInt(span_id.replace(/\D+/g,""));
        var query = $('#orig-'+rawId).text();
        $.ajax({
            url: "/ggl",
            type: "POST",
            data: {q : query},
            success: function (data) {
                $(element).text("");
                var content = '<button class="btn btn-info" onclick="editInput(';
                content += rawId + ", '" + data;
                content += '\')" >' + data + '</button>';
                if (data == "") content = "Гугл молчит =(";
                $(element).append(content);
            }
        });
    });
});