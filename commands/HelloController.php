<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\components\ApiInterface;
use app\models\Changelog;
use app\models\Groups_list;
use app\models\Groups_types;
use app\models\Tradeaccounts;
use yii\console\Controller;
use Yii;
use yii\helpers\ArrayHelper;

use app\models\Users;
use app\models\Cities;
use app\components\Translit;
use yii\db\Expression;


class HelloController extends Controller
{

    public function actionTranslit()
    {
        echo "Started!";
        $users = Users::find()->orderBy('city')->asArray();
        foreach ($users->batch(1000) as $usersSet){
            $cities = array();
            foreach ($usersSet as $user){
                $currentRaw = $user['city'];
                if (empty($currentRaw) || is_numeric($currentRaw))
                    echo "Строка не прошла проверку: ".$currentRaw.PHP_EOL;
                $city = array();
                $city['id'] = $user['ID'];
                $city['original'] = $currentRaw;
                $city['translated'] = Translit::t($currentRaw);
                $city['country_id'] = $user['country'];
                $counter = Users::find()->where(['city' => $currentRaw, 'country' => $user['country']])->count();
                $city['coincidences'] = $counter;
                array_push($cities, $city);
            }
            array_map(function($city){
                $arCity = new Cities();
                $arCity->id = $city['id'];
                $arCity->original = $city['original'];
                $arCity->translated = $city['translated'];
                $arCity->coincidences = $city['coincidences'];
                $arCity->country_id = $city['country_id'];
                $arCity->save();
            }, $cities);
            unset($users, $cities);
        }
    }

    public function actionAutocorrect(){
        $cities = Cities::find()
            ->select('cities.*, countries.rus, countries.iso2')
            ->leftJoin('countries', 'cities.country_id = countries.isoID')
            ->groupBy(new Expression('TRIM(UPPER(`original`))'))
            ->orderBy(new Expression("TRIM(original)"))
            ->asArray()->all();
        Cities::$citiesDb = $cities;
        $i = 0;
        foreach ($cities as $city){
            $current = Cities::findOne($city['id']);
            $isCorrected = ($current['corrected'] == null)? false : true;
            unset($current);
            if (!$isCorrected) {
                $geo = new ApiInterface();
                $xml = $geo->getCityXML($city['original']);
                if ( !$xml ) $xml = $geo->getCityXML($geo->findGoogle($city['original']));
                if ( !$xml || (int)$xml->status['value'] == 19 || (string)$xml->geoname->countryCode != $city['iso2'] ) continue;
                    $toponymNameXml = (string)$xml->geoname->toponymName;
                    $nameXml = (string)$xml->geoname->name;
                    $alternateNamesXml = array_map(function($val){
                        return str_replace("'","", $val);
                    } , explode(',', (string)$xml->geoname->alternateNames));
                    if (mb_strtolower($city['translated']) == mb_strtolower($toponymNameXml) || mb_strtolower(trim($city['original'])) == mb_strtolower($toponymNameXml) || mb_strtolower(trim($city['original'])) == mb_strtolower($nameXml)) {
                        Cities::updateAll(['corrected' => $toponymNameXml], ['original' => $city['original']]);
                        $suggestions = Cities::getSuggestions($city['original'], $city['country_id']);
                        foreach ($suggestions as $suggestion)
                            if (in_array($suggestion['original'], $alternateNamesXml))
                                Cities::updateAll(
                                    ['corrected' => $toponymNameXml],
                                    ['translated' => Translit::t($suggestion['original'])]
                                );
                    }
            }
            echo $city['original'].' - '.$city['id']." | ";
            echo $i.PHP_EOL;
            $i++;
        }
        echo "Готово!".PHP_EOL;
    }

    public function actionShowunsubmited(){
        echo "Ищу...".PHP_EOL;
        $query = Cities::find()->select('original, corrected')
            ->where(['is not', 'corrected', null])
            ->andWhere('saved = :false', [':false' => false]);
        $count = $query->count();
        $unsubmitedCities = $query->all();
        echo "Всего несохраненных: ".$count.PHP_EOL;
        echo "___________________________".PHP_EOL;
        foreach ($unsubmitedCities as $unsubmitedCity) {
            echo $unsubmitedCity['original'].' => '.$unsubmitedCity['corrected'].PHP_EOL;
        }
        echo "__________________________".PHP_EOL;
        echo "Всего несохраненных: ".$count.PHP_EOL;
        echo "Это список городов, названия которых скорректированны, но не отправлены в базу.".PHP_EOL;
    }

    public function actionDump(){
        $path = Yii::$app->basePath.DIRECTORY_SEPARATOR.'_dumps'.DIRECTORY_SEPARATOR.'dump_'.date("Y-m-d_H_i_s").'.sql';
        $query = "SELECT `id`, `corrected` INTO OUTFILE '$path' FROM `cities`";
        Yii::$app->db->createCommand($query)->execute();
        echo 'Done. See file: '.$path.PHP_EOL;
    }

    public function actionSubmit($arg = 1000){
        echo "Started!".PHP_EOL;
        $query = Cities::find()->select('id, corrected')
            ->where(['is not', 'corrected', null])
            ->andWhere('saved = :false', [':false' => false])
            ->limit($arg);
        foreach ($query->batch(100) as $citySet){
            $accounts = array(); // тут будет 100 пользователей, для каждого список идентификаторов его торговых аккаунтов
            foreach ($citySet as $city){
                $accountsList = array(); // аккаунты одного пользователя
                $id = $city['id'];
                $user = Users::findOne($id);
                array_push($accountsList, [ 'account' => $user['transitID'], 'type' => 'MetaTrader4' ]);
                $tradeAccounts = Tradeaccounts::find()->select('mtID, fxType')->where('userID = :id', [':id' => $id])->asArray()->all();
                foreach ($tradeAccounts as $tradeAccount){
                    $platformType = Groups_list::find()
                        ->select('groups_types.platform')
                        ->innerJoin('groups_types', 'groups_list.groupType = groups_types.id')
                        ->where('groups_list.id = :fxType', [':fxType' => $tradeAccount['fxType']])
                        ->asArray()
                        ->all();
                    $type = $platformType[0]['platform'];
                    array_push($accountsList, [ 'account' => $tradeAccount['mtID'], 'type' => $type ]);
                }
                array_push($accounts, [$id => $accountsList, 'corrected' => $city['corrected']]);
                unset($accountsList);
            }
            foreach ($accounts as $item){
                $corrected = $item['corrected'];
                unset($item['corrected']);
                foreach ($item as $key => $value){ // на самом деле тут всего 1 элемент и foreach сработает 1 раз
                    $user = Users::findOne($key);
                    $user->city = $corrected;
                    $user->update();
                    $dbCurrentCity = Cities::findOne($key);
                    $dbCurrentCity->saved = true;
                    $dbCurrentCity->update();
                    foreach ($value as $account){ // проход по всем торговым аккаунтам текущего пользователя
                        //$mt = MTWrapper::factory($account['type']);
                        //$update = $mt->userRecordUpdate($account['account'], ['city' => $corrected]);
                        //$status = ($update)? true : false;
                        Changelog::log($key, $account['account'], $corrected, $status = false);
                    }
                }
            }
            unset($accounts);
        }
    }

}
