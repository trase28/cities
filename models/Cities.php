<?php
/**
 * Created by PhpStorm.
 * User: trase
 * Date: 24.04.17
 * Time: 16:49
 */

namespace app\models;
use app\components\ApiInterface;
use yii\db\ActiveRecord;
use app\components\Translit;
use yii\db\Expression;
use yii\helpers\ArrayHelper;


class Cities extends ActiveRecord{
    public static $citiesDb = array();
    public static function normalizeCity($queryCity, $queryCountryId){
        $queryCity = preg_replace(array('/(^|\s)(обл|п|c|г|р-н|АО|край|респ|гор|район|село|пос|дер|поселок|деревня|де|город|республика|кр|к|г-д|obl|p|c|g|r-n|AO|kraj|resp|gor|rajon|selo|pos|der|poselok|derevnja|de|gorod|respublika|kr|k|g-d)(\.|\s|$)/i', '/\s(обл|п|c|г|р-н|АО|край|респ|гор|район|село|пос|дер|поселок|деревня|де|город|республика|кр|к|г-д|obl|p|c|g|r-n|AO|kraj|resp|gor|rajon|selo|pos|der|poselok|derevnja|de|gorod|respublika|kr|k|g-d)\s/i'), '', $queryCity);
        $geo = new ApiInterface();
        $onlyDigits = preg_replace('/\D+/','',$queryCity);
        if ( (is_numeric(trim($queryCity)) || strlen($queryCity) <= 3) || ( strlen($onlyDigits) > 3 ) ){
            switch ($queryCountryId){
                case '643':
                    $geoCountryFormat = 'RU';
                    break;
                case '804':
                    $geoCountryFormat = 'UA';
                    break;
                case '112':
                    $geoCountryFormat = 'BY';
                    break;
                default:
                    $geoCountryFormat = 'RU';
                    break;
            }
            $geoSuggestion = $geo->findByIndex(trim($queryCity), $geoCountryFormat);
            return $geoSuggestion;
        }
        $lemmaFound = false;
        $lemms = array('b', 'd', 'yt', 'jy', 'yf', 'z', 'xnj', 'c', 'cj', 'njn', ',snm', 'f', 'dtcm',
            "'nj", 'rfr', 'jyf', 'gj', 'yj', 'jyb', 'r', 'e', 'ns', 'bp', 'pf', 'ds', 'nfr', ';t', 'jn',
            'crfpfnm',"'njn", 'rjnjhsq', 'vjxm', 'xtkjdtr', 'j', 'jlby', 'tot', ',s', 'nfrjq', 'njkmrj',
            'ct,z', 'cdjt', 'rfrjq', 'rjulf', 'e;t', 'lkz', 'djn', 'rnj', 'lf', 'ujdjhbnm', 'ujl', 'pyfnm',
            'vjq', 'lj', 'bkb', 'tckb', 'dhtvz', 'herf', 'ytn', 'cfvsq', 'yb', 'cnfnm', ',jkmijq', 'lf;t', 'lheujq',
            'yfi', 'cdjq', 'ye', 'gjl', 'ult', 'ltkj', 'tcnm', 'cfv', 'hfp', 'xnj,s', 'ldf', 'nfv', 'xtv', 'ukfp',
            ';bpym', 'gthdsq', 'ltym', 'nenf', 'ybxnj', 'gjnjv', 'jxtym', '[jntnm', 'kb', 'ghb', 'ujkjdf', 'yflj',
            ',tp', 'dbltnm', 'blnb', 'ntgthm', 'nj;t', 'cnjznm', 'lheu', 'ljv', 'ctqxfc', 'vj;yj', 'gjckt', 'ckjdj',
            'pltcm', 'levfnm', 'vtcnj', 'cghjcbnm', 'xthtp', 'kbwj', 'njulf', 'dtlm', '[jhjibq', 'rf;lsq', 'yjdsq',
            ';bnm', 'ljk;ys', 'cvjnhtnm', 'gjxtve', 'gjnjve', 'cnjhjyf', 'ghjcnj', 'yjuf', 'cbltnm', 'gjyznm', 'bvtnm',
            'rjytxysq', 'ltkfnm', 'dlheu', 'yfl', 'dpznm', 'ybrnj', 'cltkfnm', 'ldthm', 'gthtl', 'ye;ysq', 'gjybvfnm',
            'rfpfnmcz', 'hf,jnf', 'nhb', 'dfi', 'e;', 'ptvkz', 'rjytw',
            'ytcrjkmrj', 'xfc', 'ujkjc', 'ujhjl', 'gjcktlybq',
            'gjrf', '[jhjij', 'ghbdtn', 'pljhjdj', 'pljhjdf', 'ntcn', 'yjdjq', 'jr', 'tuj', 'rjt', 'kb,j', 'xnjkb',
            'ndj.', 'ndjz', 'nen', 'zcyj', 'gjyznyj', 'x`', 'xt');
        foreach ($lemms as $lemma){
            if ( strpos($queryCity, $lemma) !== false ) { $lemmaFound = true; break; }
        }

        if ($lemmaFound && ((int)$queryCountryId == 643 || (int)$queryCountryId == 804)){
            $queryCity = Translit::r($queryCity);
            $queryCity = Translit::t(trim($queryCity));
            return $queryCity;
        }
        $queryCity = mb_convert_case(trim($queryCity), MB_CASE_TITLE, "UTF-8");
        return $queryCity;
    }

    public static function getSuggestions($queryCity, $queryCountryId){
        $normalizedCity = self::normalizeCity($queryCity, $queryCountryId);
        $queryCity = Translit::t($queryCity);
        $samples = array_filter(self::$citiesDb, function ($var) use ($queryCity, $queryCountryId){
            if (
                $var['country_id'] !==  $queryCountryId ||
                ord($var['translated']) != ord($queryCity) ||
                abs(strlen($var['translated']) - strlen($queryCity)) > 3
            ) return false; else return true;
        });
        ArrayHelper::multisort($samples, 'translated');
        $suggestions = array();
        foreach ($samples as $sample) {
            $lev = levenshtein($queryCity, $sample['translated']);
            $sim = similar_text($queryCity, $sample['translated']);
            $soundsLike = ( abs(metaphone($queryCity) - metaphone($sample['translated'])) <= 1 )? true : false;
            $rating = (int)$sample['coincidences'];
            if ( $lev <= 2 && abs(strlen($queryCity) - $sim) <= 2 && $soundsLike) {
                $original = mb_convert_case(trim($sample['original']), MB_CASE_TITLE, "UTF-8");
                array_push($suggestions, ['original' => $original, 'rating' => $rating]);
            }
        }
        ArrayHelper::multisort($suggestions, 'rating', SORT_DESC);
        if ((int)$queryCountryId == 643 || (int)$queryCountryId == 804){
            $rusSuggestions = array(); $noRusSuggestions = array();
            foreach ($suggestions as $suggestion) preg_match("/[а-яА-Я]/i", $suggestion['original'])? array_push($rusSuggestions, $suggestion) : array_push($noRusSuggestions, $suggestion);
            $suggestions = array_merge($rusSuggestions, $noRusSuggestions);
        }
        if ($normalizedCity != "" && $normalizedCity !== false) array_push($suggestions, ['original' => $normalizedCity, 'rating' => 0]);
        $unicStack = array(); $countSuggestions = count($suggestions);
        for ($i = 0; $i < $countSuggestions; $i++){
            if ( ArrayHelper::isIn($suggestions[$i]['original'], $unicStack) ) unset($suggestions[$i]);
            else array_push($unicStack, $suggestions[$i]['original'] );
        }
        return $suggestions;
    }
    public static function saveSingle($id, $val){
        $val = mb_convert_case(Translit::t($val), MB_CASE_TITLE, "UTF-8");
        $translated = self::findOne($id);
        $translated = $translated['translated'];
        self::updateAll(['corrected' => $val], ['translated' => $translated] );
    }
    public static function saveFromJSON($json){
        $data = json_decode($json, true);
        foreach ($data as $datum) self::saveSingle($datum['id'], $datum['val']);
    }

    public static function getCorrected($asArray = false){
        $query = self::find()->select('id, corrected')->where(['is not', 'corrected', null]);
        if ($asArray) $query = $query->asArray();
        $query = $query->all();
        return $query;
    }
}