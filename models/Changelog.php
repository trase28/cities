<?php
/**
 * Created by PhpStorm.
 * User: trase
 * Date: 24.04.17
 * Time: 16:49
 */

namespace app\models;
use yii\db\ActiveRecord;



class Changelog extends ActiveRecord{

    public static function log($userId, $mtID, $newValue, $status){
        $date = date("Y-m-d H:i:s");
        $enity = new self();
        $enity->dateTime = $date;
        $enity->mtID = $mtID;
        $enity->newValue = $newValue;
        $enity->status = $status;
        $enity->userId = $userId;
        $enity->save();
    }
}