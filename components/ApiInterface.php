<?php

namespace app\components;
use yii\httpclient\Client;
use phpQuery;


class ApiInterface
{
    private $geoAccounts = ["trase8", "trase8_2", "trase8_3", "trase8_4", "trase8_5"];
    private $gglKeys = ['AIzaSyCi5ILJq9qMknPECZb2-w0hR0dNxLQ0FWw', 'AIzaSyCAWFYDiFedskpl6puh7breQSyXyRqEtKk'];

    public function findByIndex($index, $country = 'RU'){
        $client = new Client(['baseUrl' => 'http://www.geonames.org']);
        $response = $client->get('postalcode-search.html', [
            'q' => $index,
            'country' => $country,
        ])->send();
        if ($response->isOk) {
            $html = $response->content;
            $pq = phpQuery::newDocument($html);
            $elem = $pq->find('table.restable > tr:nth-child(2) > td::nth-child(2)');
            $targetData = preg_replace('/\d/','', $elem->html());
            return trim($targetData);
        }
    }

    public function findByQuery($query, $country = 'RU'){
        $client = new Client(['baseUrl' => 'http://www.geonames.org']);
        $response = $client->get('advanced-search.html', [
            'q' => $query,
            'country' => $country,
            'featureClass' => 'P',
            'fuzzy' => '0.4'
        ])->send();
        if ($response->isOk) {
            $html = $response->content;
            $pq = phpQuery::newDocument($html);
            $elem = $pq->find('table.restable >  tr:nth-child(3) >  td:nth-child(2) > a:first-child');
            return trim($elem->html());
        }
    }

    public function getCityXML($city){
        $client = new Client(['baseUrl' => 'http://api.geonames.org/']);
        $user = $this->geoAccounts[array_rand($this->geoAccounts, 1)];
        $response = $client->get('search', [
            'maxRows' => 1,
            'username' => $user,
            'style' => 'full',
            'q' => trim($city),
        ])->send();
        if ($response->isOk) {
            $xml = new \SimpleXMLElement($response->content);
            return $xml;
        } else return false;
    }

    public function findGoogle($query){
        $client = new Client(['baseUrl' => 'https://maps.googleapis.com/maps/api/geocode']);
        $key = $this->gglKeys[array_rand($this->gglKeys, 1)];
        $response = $client->get('json', [
            'address' => $query,
            'key' => $key,
        ])->send();
        if ($response->isOk) {
            $json = json_decode($response->content, true);
            if ($json['status'] == "OK"){
                return $json['results'][0]['address_components'][0]['long_name'];
            } else return false;
        }
    }

}